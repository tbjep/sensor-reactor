use std::sync::{Arc, Mutex};

use clap::Parser;
use futures::prelude::*;
use stream::TryStreamExt;

use tokio::io::{AsyncBufReadExt, AsyncRead, BufReader};
use tokio::process::Command;
use tokio_serial::SerialPortBuilderExt;
use tokio_stream::wrappers::LinesStream;

type BoxError = Box<dyn std::error::Error + Send + Sync + 'static>;
type BoxErrorResult<T> = Result<T, BoxError>;

async fn run_command(
    mutex: Arc<Mutex<()>>,
    comfac: Arc<impl Fn() -> Command>,
) -> BoxErrorResult<()> {
    let lock = mutex.try_lock();
    if let Ok(_) = lock {
        comfac()
            .spawn()?
            .wait()
            .await?;
    }
    Ok(())
}

async fn react<R: AsyncRead>(
    reader: BufReader<R>,
    comfac: Arc<impl Fn() -> Command>,
) -> BoxErrorResult<()> {
    let mutex = Arc::new(Mutex::new(()));
    let stream = LinesStream::new(reader.lines());
    stream
        .map_err(BoxError::from)
        .try_for_each_concurrent(None, move |v| {
            let mutex = mutex.clone();
            let comfac = comfac.clone();
            async move {
                if v.starts_with("0") {
                    run_command(mutex, comfac.clone()).await
                } else {
                    Ok(())
                }
            }
        })
        .await
}

fn command_factory(command: String) -> impl Fn() -> Command {
    move || {
        let mut result = Command::new("sh");
        result.arg("-c").arg(command.clone());
        result
    }
}

#[derive(Parser)]
struct Args {
    /// Name of the person to greet
    #[clap(short, long, value_parser)]
    command: String,

    #[clap(short, long, value_parser)]
    device: String,

    #[clap(short, long, value_parser, default_value_t = 9600)]
    baud_rate: u32,
}

#[tokio::main]
async fn main() -> BoxErrorResult<()> {
    let args = Args::parse();
    println!("{}", args.command);
    
    let serial = tokio_serial::new(args.device, args.baud_rate)
        .open_native_async()
        .unwrap();
    let reader = BufReader::new(serial);
    react(
        reader,
        Arc::new(command_factory(
            args.command
                .to_string(),
        )),
    )
    .await?;
    Ok(())
}
